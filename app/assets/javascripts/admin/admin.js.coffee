#= require query-2.1.1.min
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require_tree ./views
#= require_tree ./controllers
#= require_tree ./templates
#= require_tree ./routes

window.Fb = Ember.Application.create()

Fb.IndexView = Em.View.extend
  templateName: 'admin/admin'
