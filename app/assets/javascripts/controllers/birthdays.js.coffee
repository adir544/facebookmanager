Fb.BirthdaysController = Em.ArrayController.extend
  months: (->
    currentYear = (new Date()).getFullYear()
    users = @get('content')
    [1..12].map (month) ->
      m = moment("#{currentYear}-#{month}-01", 'YY-MM-DD')
      days = [1..m.daysInMonth()].map (day) ->
        birthdayUsers = users.filter (user) ->
          (user.birthday.month() == month - 1 and
          user.birthday.date() == day)
        return {
          number: day,
          users: if birthdayUsers.length then birthdayUsers else null
        }

      return {
        number: month,
        name: m.format('MMMM'),
        days: days,
        startIn: m.weekday()
      }
  ).property()

  spacerClass: (->
    "spacer-#{@get('selectedMonth.startIn')}"
  ).property('selectedMonth')

  onSelectedMonthChange: (->
    # Highlight birthdays
    Em.run.scheduleOnce 'afterRender', this, =>
      @get('selectedMonth.days').forEach (day) ->
        if day.users
          $(".month input[id='radio-#{day.number-1}']").addClass('has-birthday')
    # Deselect day
    @set('selectedDay', null)
  ).observes('selectedMonth')
