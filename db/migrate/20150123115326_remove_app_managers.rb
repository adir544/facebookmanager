class RemoveAppManagers < ActiveRecord::Migration
  def change
    AppManager.find_each do |app_manager|
      # For each AppMaanger lets update the corresponding AppUser type
      app_user = AppUser.find_by_user_id(app_manager.manager_id)
      app_user.update! user_type: 'manager' if app_user.present?
    end

    # Now we can get rid of AppManagers
    drop_table :app_managers
  end
end
