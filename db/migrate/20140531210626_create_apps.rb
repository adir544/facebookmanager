class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :name, null: false
      t.string :fb_app_id, null: false
      t.string :fb_app_secret, null: false
      t.string :fb_app_return_url, null: false

      t.timestamps
    end
  end
end
