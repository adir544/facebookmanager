class JobSerializer < ActiveModel::Serializer
  attributes :id, :manager_id, :action_name, :action_params, :job_state, :created_at

  def created_at
    object.created_at.to_i
  end
end
