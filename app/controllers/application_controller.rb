class ApplicationController < ActionController::Base
  def current_app_user
    AppUser.find_by_id(session['app_user_id'])
  end
  
  def current_user
    app_user = current_app_user
    if app_user.present?
      app_user.user
    end
  end
end
