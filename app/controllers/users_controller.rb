class UsersController < LoggedController

  def show
    render json: current_app_user, serializer: AppUserSerializer
  end

  def update
    current_app_user.update_info
    current_app_user.update_fb_data
    render json: current_user
  end

  def destroy
    user = current_user.app.users.find(params[:id])
    user.delete
    render nothing: true
  end

  def change_type
    new_user_type = params[:user_type]
    unless ['normal', 'manager'].include? new_user_type
      render text: 'Invalid user type requested', status: :unprocessable_entity
      return
    end
    app_user = current_app_user.app.app_users.find_by_user_id!(params[:user_id])
    app_user.update! user_type: new_user_type
    render nothing: true
  end

  def perform
    # Delay validation
    delay_range = 0..1440
    delay = Float(params[:delay] || 0)
    raise 'delay is not in range' unless delay_range.include? delay

    # Delay between users validation
    delay_between_users_range = 30..600
    delay_between_users = Float(params[:delay_between_users] || delay_between_users_range.min)
    raise 'delay_between_users is not in range' unless delay_between_users_range.include? delay_between_users

    # App user IDs validation
    app_user_ids = params.fetch(:app_user_ids, '').split(',')
    raise 'no app user ids provided' if app_user_ids.empty?
    app_users = current_app_user.app.app_users.find(app_user_ids)
    raise 'no app users found' if app_users.empty?

    action_name = params.require(:action_name)
    action_params = {}
    case action_name
    when 'post_status'
      action_params[:message] = params.require :message
    when 'like_statuses'
      action_params[:status_ids] = params.require :status_ids
    when 'like_comment'
      action_params[:comment_id] = params.require :comment_id
    when 'like_video'
      action_params[:video_id] = params.require :video_id
    when 'like_photos'
      action_params[:photo_ids] = params.require :photo_ids
    when 'attend_event'
      action_params[:event_id] = params.require :event_id
    when 'post_photo'
      action_params[:message] = params[:message]
      action_params[:filename] = save_photo(params.require :photo)
    else
      raise 'unsupported action requsted'
    end

    # Create a new job
    job = Job.create!(
      manager: current_app_user,
      job_state: 'queued',
      action_name: action_name,
      action_params: action_params,
      job_delay: delay,
      delay_between_users: delay_between_users
    )
    job.app_users << app_users

    # The actual worker dispatch should happend only in development and production
    if Rails.env.development? or Rails.env.production?
      if delay > 0
        FacebookWorker.perform_in(delay.minutes, job.id)
      else
        FacebookWorker.perform_async(job.id)
      end
    end
    render nothing: true
  end

  def birthdays
    birthdays = User.select 'id, name, birthday, picture_url'
    render json: birthdays.to_json
  end

  private

  # save the uploaded file to the server and returns the generated filename
  def save_photo(uploaded_file)
    supported_photo_formats = ['.jpg', '.jpeg', '.png']
    raise 'No photo uploaded' if uploaded_file.blank?

    file_ext = File.extname(uploaded_file.original_filename).downcase
    unless supported_photo_formats.include?(file_ext)
      raise "Unsupported format was uploaded, supported: #{supported_photo_formats.to_s}"
    end

    file_name = Dir::Tmpname.make_tmpname([current_user.id.to_s, file_ext], nil)
    photos_directory = File.join(Rails.root, 'public', 'photos')
    Dir.mkdir(photos_directory) unless Dir.exists? photos_directory
    file_path = File.join(photos_directory, file_name)
    File.open(file_path, 'wb') {|f| f.write(uploaded_file.read)}

    file_name
  end
end
