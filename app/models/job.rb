class Job < ActiveRecord::Base
  belongs_to :manager, class_name: AppUser
  has_and_belongs_to_many :app_users
end
