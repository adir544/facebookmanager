class AddFbTokenCreatedAtColumnToAppUsers < ActiveRecord::Migration
  def change
    add_column :app_users, :fb_token_created_at, :datetime
  end
end
