Fb.UserSelectorComponent = Em.Component.extend
  layoutName: 'components/user_selector'

  disabled: false

  _register: (->
    @set('register', this)
  ).on('init')
  
  _users: (->
    users = @getWithDefault('users', [])
    users.map (user) =>
      Em.ObjectProxy.create(
        content: user
        checked: @get('selectedUsers')?.contains(user) or false
        checkboxId: "#{@get('name')}_user#{user.get('id')}"
      )
  ).property('users')

  userChecked: (->
    selectedUsers = @get('_users').filterBy('checked').mapBy('content')
    @set('selectedUsers', selectedUsers)
    return
  ).observes('_users.@each.checked')

  selectAll: ->
    @get('_users').filterBy('checked', false).forEach (user) ->
      user.set('checked', true)
    return

  selectNone: ->
    @get('_users').filterBy('checked', true).forEach (user) ->
      user.set('checked', false)
    return
