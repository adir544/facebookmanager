Fb.JobsView = Em.View.extend
  didInsertElement: ->
    jobs = document.querySelectorAll('.job')
    for job in jobs
      job.addEventListener 'click', (evt) ->
        $(evt.target.closest('.job')).toggleClass('open')
