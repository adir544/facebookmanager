Fb.LikeVideoView = Fb.ActionView.extend
  templateName: 'like_video'

  valid: (->
    if !(@get('controller.loggedUser.fbData.videos.length') > 0)
      return false
    $('form')[0].checkValidity()
  ).property('controller.loggedUser.fbData.videos.[]', 'controller.box.selectedVideo')

  userHasVideos: (->
    loggedUser = @get('controller.loggedUser')
    (loggedUser.get('fbData.videos.length') > 0 or
    loggedUser.get('fbData.tagged_videos.length') > 0)
  ).property('controller.loggedUser.fbData.{videos,tagged_videos}')

  userVideoAlbums: (->
    loggedUser = @get('controller.loggedUser')
    [
      {
        name: 'Tagged videos'
        videos: loggedUser.get('fbData.tagged_videos')
      },
      {
        name: 'Uploaded videos'
        videos: loggedUser.get('fbData.videos')
      }
    ]
  ).property('controller.loggedUser.fbData.{videos,tagged_videos}')
