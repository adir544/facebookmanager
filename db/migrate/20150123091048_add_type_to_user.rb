class AddTypeToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_type, :string, {nil: false, default: 'normal'}
  end
end
