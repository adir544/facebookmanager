class GroupPersonSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :group_id
  has_one :person
end
