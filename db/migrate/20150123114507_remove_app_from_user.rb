class RemoveAppFromUser < ActiveRecord::Migration
  def change
    User.find_each do |user|
      # For each user lets add a new AppUser row.
      AppUser.create!({
        user_id: user.id,
        app_id: user.app_id,
        fb_token: user.fb_token
      })
    end

    # Remove the app_id from users and fb_token as they
    # no longer relevent to a specific user.
    remove_belongs_to :users, :app
    remove_column :users, :fb_token
  end
end
