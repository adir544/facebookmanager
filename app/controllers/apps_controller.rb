class AppsController < LoggedController
  def show
    if current_app_user.user_type == 'manager' or current_user.user_type == 'admin'
      serializer = current_user.user_type == 'admin' ? AdminAppSerializer : AppSerializer
      render json: current_app_user.app, serializer: serializer
    else
      render text: 'Access denied'
    end
  end
end
