class AppSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :name, :statuses, :photos
  has_many :app_users, each_serializer: BasicAppUserSerializer

  def statuses
    messages = object.statuses.group_by {|s| s.message}
    messages.map do |message, statuses|
      message = "#{message} [via #{statuses.count} people]" if statuses.count > 1
      {
        message: message,
        id: statuses.map(&:fb_id).join(',')
      }
    end.reverse
  end

  def photos
    photos = object.photos.group_by {|p| p.filename}
    photos.map do |filename, photos|
      {
        id: photos.map(&:fb_id).join(','),
        message: photos.first.message,
        picture: "photos/#{filename}"
      }
    end.reverse
  end
end
