Fb.ActionsMixin = Em.Mixin.create
  # TODO: maybe move this to mixins
  _ajaxPost: (url, postData) ->
    new Em.RSVP.Promise (resolve, reject) ->
      $.ajax(
        type: 'POST'
        url: url
        data: postData
        contentType: false
        processData: false
      ).then(resolve, reject)
      return

  _postAction: (actionName, formData) ->
    $.blockUI()

    # append the action type and selected user ids
    formData.append('action_name', actionName)
    formData.append('app_user_ids', @get('controller.selectedUsers').mapBy('id'))
    formData.append('delay', @get('controller.delay'))
    formData.append('delay_between_users', @get('controller.delayBetweenUsers'))

    eventTarget = $('.review button')
    @_ajaxPost('/user/perform', formData).then( =>
      $('header').notify('Done', {className: 'success', position: 'bottom right'})
      @controllerFor('panel').set('currentStep', 'select-action')
      @transitionTo 'completed'
    , (ex) ->
      $(eventTarget).notify("error: #{ex}", 'error')
    ).then( ->
      $.unblockUI()
    )

  actions:
    postStatus: ->
      formData = new FormData()
      formData.append('message', @get('controller.box.statusMessage'))
      @_postAction 'post_status', formData
      return

    postPhoto: ->
      formData = new FormData()
      formData.append('photo', @get('controller.box.photoFile'))
      photoMessage = @get('controller.box.photoMessage')
      if photoMessage?
        formData.append('message', photoMessage)
      @_postAction 'post_photo', formData
      return

    likeStatus: ->
      formData = new FormData()
      formData.append('status_ids', @get('controller.box.selectedStatus.id'))
      @_postAction 'like_statuses', formData
      return

    attendEvent: ->
      formData = new FormData()
      formData.append('event_id', @get('controller.box.selectedEvent.id'))
      @_postAction 'attend_event', formData
      return

    likeComment: ->
      formData = new FormData()
      formData.append('comment_id', @get('controller.box.selectedComment.id'))
      @_postAction 'like_comment', formData
      return

    likePhoto: ->
      formData = new FormData()
      formData.append('photo_ids', @get('controller.box.selectedPhoto.id'))
      @_postAction 'like_photos', formData
      return

    likeVideo: ->
      formData = new FormData()
      formData.append('video_id', @get('controller.box.selectedVideo.id'))
      @_postAction 'like_video', formData
      return
