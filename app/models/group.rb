class Group < ActiveRecord::Base
  has_and_belongs_to_many :app_users
  has_many :users, through: :app_users
  belongs_to :owner_app_user, class_name: :AppUser
end
