Fb.BirthdaysRoute = Em.Route.extend
  model: ->
    $.getJSON('birthdays').then (users) ->
      users.forEach (user) ->
        m = moment(user.birthday)
        user.birthday = m
        user.formatted_birthday = m.format('DD.MM.YYYY')
      users

  setupController: (controller, model) ->
    @_super(controller, model)

    # Select current month
    thisMonth = (new Date()).getMonth() + 1
    currentMonth = controller.get('months').find (month) ->
      month.numbur == thisMonth
    controller.set('selectedMonth', currentMonth)
    return
