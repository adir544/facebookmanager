class BasicUserSerializer < ActiveModel::Serializer
  attributes :id, :name, :fb_id, :picture_url, :created_at, :user_type

  def created_at
    object.created_at.to_i
  end
end
