#!/bin/bash
function wait_for {
  echo "Waiting for '$1'"

  while (true) do
    ping -c1 $1
    if [ $? -eq 0 ] ; then
      break
    else
      sleep 1
    fi
  done

  echo "'$1' is up"
}

wait_for facebookmanager-backend

nginx -g 'daemon off;'
