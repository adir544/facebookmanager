class RemoveFbAppReturnUrlFromApps < ActiveRecord::Migration
  def change
    remove_column :apps, :fb_app_return_url
  end
end
