class User < ActiveRecord::Base
  has_many :app_users
  has_many :apps, through: :app_users

  def admin?
    user_type == 'admin'
  end
end
