require 'facebook'

class FacebookWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
  ACTIONS = %w(post_status like_statuses like_comment like_photos post_photo attend_event like_video)

  def perform(job_id)
    # Check if the job exists
    @job = Job.find_by_id(job_id)
    if @job.nil?
      # This is a weird case, but still better to check
      return error "Job ##{job_id} doesn't exists"
    end

    logger.info "Checking Job ##{@job.id}"
    @job.update! sidekiq_jid: jid

    # Check if the requested action is available
    unless ACTIONS.include? @job.action_name
      error "Unsupported action(#{@job.action_name}) was requested"
      @job.update! job_state: 'invalid'
      # TODO: Add log event with the error message
      return
    end

    # Check if the initiator has permissions
    initiator = @job.manager
    if initiator.user_type != 'manager' and initiator.user.user_type != 'admin'
      error "Intiator is not an app manager/admin,
      user_type=#{initiator.user_type}/#{initiator.user.user_type}"
      @job.update! job_state: 'invalid'
      # TODO: Add log event with the error message
      return
    end

    # Update the job state to in_progress
    @job.update! job_state: 'in_progress'

    # Log information about this job
    logger.info({
      app_name: @job.manager.app.name,
      manager_name: @job.manager.user.name,
      job: @job.serializable_hash
    })

    # Start working
    self.send(@job.action_name)

    # Update the job state to done
    @job.update! job_state: 'done'
  end

  private

  def error(message)
    if Rails.env.test?
      raise RuntimeError.new(message)
    else
      logger.error message
    end
  end

  def process_action(&action_code)
    app_users = @job.app_users
    app_users.each do |app_user|
      app_user_rep = "AppUser ##{app_user.id} (#{app_user.user.name})"
      begin
        logger.info "Calling #{caller_locations[3].label} with #{app_user_rep}"
        # TODO: Add log event
        action_code.call(app_user)
        logger.info "Processing #{app_user_rep} completed successfully"
        # TODO: Add log event
      rescue
        error "Processing #{app_user_rep} failed. Error message: #{$!.message}"
        # TODO: Add log event
      end
      # Sleep between users unless we're processing the last user
      # in the list
      unless app_user == app_users.last
        delay = @job.delay_between_users
        logger.info "Sleeping #{delay} seconds"
        sleep delay
      end
    end
  end

  def post_status
    process_action do |app_user|
      status_id = app_user.post_status @job.action_params['message']
      Status.create!(
        fb_id: status_id,
        user_id: app_user.user_id,
        app_id: @job.manager.app.id,
        message: @job.action_params['message'],
        posted_at: Time.now
      )
    end
  end

  def like_statuses
    status_ids = @job.action_params['status_ids'].split(',')
    process_action do |app_user|
      status_ids.each do |status_id|
        begin
          app_user.like(status_id)
        rescue
          logger.error "Failed to like status ##{status_id} with #{app_user}, error: #{$!.message}"
          next
        end
        app_user_rep = "AppUser ##{app_user.id} (#{app_user.user.name})"
        logger.info "#{app_user_rep} likes status ##{status_id}"
        unless status_id == status_ids.last
          delay = @job.delay_between_users
          logger.info "Sleeping #{delay} seconds"
          sleep delay
        end
      end
    end
  end

  def like_comment
    process_action do |app_user|
      app_user.like @job.action_params['comment_id']
    end
  end

  def like_video
    process_action do |app_user|
      app_user.like @job.action_params['video_id']
    end
  end

  def like_photos
    photo_ids = @job.action_params['photo_ids'].split(',')
    process_action do |app_user|
      photo_ids.each do |photo_id|
        begin
          app_user.like(photo_id)
        rescue
          logger.error "Failed to like photo ##{photo_id} with #{app_user}, error: #{$!.message}"
          next
        end
        app_user_rep = "AppUser ##{app_user.id} (#{app_user.user.name})"
        logger.info "#{app_user_rep} likes photo ##{photo_id}"
        unless photo_id == photo_ids.last
          delay = @job.delay_between_users
          logger.info "Sleeping #{delay} seconds"
          sleep delay
        end
      end
    end
  end

  def attend_event
    process_action do |app_user|
      app_user.attend_event @job.action_params['event_id']
    end
  end

  def post_photo
    process_action do |app_user|
      photo_url = "https://socialmanager.club/photos/#{@job.action_params['filename']}"
      photo_id = app_user.post_photo(photo_url, @job.action_params['message'])
      app_user.app_photos.create!(
        fb_id: photo_id,
        filename: @job.action_params['filename'],
        message: @job.action_params['message'],
        posted_at: Time.now
      )
    end
  end

end
