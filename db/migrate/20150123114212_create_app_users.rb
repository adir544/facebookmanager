class CreateAppUsers < ActiveRecord::Migration
  def change
    create_table :app_users do |t|
      t.references :user, nil: false
      t.references :app, nil: false
      t.string :user_type, {nil: false, default: 'normal'}
      t.string :fb_token, nil: false
    end
  end
end
