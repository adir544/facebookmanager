Fb.PostStatusView = Em.View.extend
  templateName: 'post_status'

  valid: (->
    $('form')[0].checkValidity()
  ).property('controller.box.statusMessage')
