require 'test_helper'
require 'webmock/minitest'

class FacebookWorkerTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  def setup
    @app = create(:app)

    # User is the manager, respect him
    @user = create(:user)
    @app_user = AppUser.create!({
      user: @user,
      app: @app,
      user_type: 'manager'
    })

    # Another user is a sucker
    @another_user = create(:user)
    @another_app_user = AppUser.create!({
      user: @another_user,
      app: @app,
      user_type: 'normal'
    })

    @worker = FacebookWorker.new
  end

  test "should post status" do
    stub_request(:post, /.*graph.facebook.com.*/).to_return(body: {id: 11}.to_json)
    @worker.perform('post_status', @app_user.id, [@app_user.id], {'message' => 'hey'})
  end

  test "should like status" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_statuses', @app_user.id, [@app_user.id], {'status_ids' => 'single'})
  end

  test "should like multiple statuses" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_statuses', @app_user.id, [@app_user.id, @another_app_user.id], {'status_ids' => '1,2,3'})
  end

  test "should like comment" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_comment', @app_user.id, [@app_user.id], {'comment_id' => '1'})
  end

  test "should like video" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_video', @app_user.id, [@app_user.id], {'video_id' => '1'})
  end

  test "should like photo" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_photos', @app_user.id, [@app_user.id], {'photo_ids' => '1'})
  end

  test "should like multiple photos" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('like_photos', @app_user.id, [@app_user.id, @another_app_user.id], {'photo_ids' => '1,2,3'})
  end

  test "should attend eveent" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('attend_event', @app_user.id, [@app_user.id], {'photo_id' => '1'})
  end

  test "should post photo" do
    stub_request(:post, /.*graph.facebook.com.*/)
    @worker.perform('post_photo', @app_user.id, [@app_user.id], {'filename' => 'a.jpg', 'message' => 'asd'})
  end
end
