Fb.PanelIndexRoute = Em.Route.extend
  model: ->
    @modelFor 'panel'

  actions:
    continue: ->
      @transitionTo 'panel.select_users'
      return
