class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.belongs_to :app, null: false
      t.string :name, null: false
      t.string :fb_id, null: false
      t.string :fb_token, null: false
      t.string :picture_url, null: false
      t.text :fb_data

      t.timestamps
    end
  end
end
