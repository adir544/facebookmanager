class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :fb_id, null: false
      t.references :app_user, index: true, null: false
      t.string :filename, null: false
      t.string :message
      t.datetime :posted_at, null: false

      t.timestamps
    end
  end
end
