# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150223154918) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "app_id"
    t.string   "user_type",           default: "normal"
    t.string   "fb_token"
    t.datetime "fb_token_created_at"
  end

  create_table "app_users_groups", id: false, force: true do |t|
    t.integer "group_id"
    t.integer "app_user_id"
  end

  create_table "app_users_jobs", id: false, force: true do |t|
    t.integer "job_id",      null: false
    t.integer "app_user_id", null: false
  end

  create_table "apps", force: true do |t|
    t.string   "name",          null: false
    t.string   "fb_app_id",     null: false
    t.string   "fb_app_secret", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name",              null: false
    t.integer  "owner_app_user_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.integer  "manager_id",          null: false
    t.string   "job_state",           null: false
    t.string   "action_name",         null: false
    t.json     "action_params",       null: false
    t.float    "job_delay",           null: false
    t.float    "delay_between_users", null: false
    t.string   "sidekiq_jid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "jobs", ["manager_id"], name: "index_jobs_on_manager_id", using: :btree

  create_table "photos", force: true do |t|
    t.string   "fb_id",       null: false
    t.integer  "app_user_id", null: false
    t.string   "filename",    null: false
    t.string   "message"
    t.datetime "posted_at",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "photos", ["app_user_id"], name: "index_photos_on_app_user_id", using: :btree

  create_table "statuses", force: true do |t|
    t.string   "fb_id"
    t.integer  "user_id"
    t.integer  "app_id"
    t.text     "message"
    t.datetime "posted_at"
  end

  create_table "users", force: true do |t|
    t.string   "name",                           null: false
    t.string   "fb_id",                          null: false
    t.string   "picture_url",                    null: false
    t.text     "fb_data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "birthday"
    t.string   "user_type",   default: "normal"
  end

end
