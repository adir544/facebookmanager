class JobsController < LoggedController
  def index
    manager_ids = current_app_user.app.app_users.where(user_type: 'manager')
    render json: Job.where(manager_id: manager_ids).order('id DESC')
  end
end
