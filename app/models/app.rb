class App < ActiveRecord::Base
  has_many :app_users
  has_many :users, through: :app_users
  has_many :statuses
  has_many :photos, through: :app_users, source: :app_photos
end
