Fb.PanelController = Em.ObjectController.extend
  needs: 'application'

  indexActive: Em.computed.equal('controllers.application.currentPath', 'panel.index')
  selectUsersActive: Em.computed.equal('controllers.application.currentPath', 'panel.select_users')
  reviewActive: Em.computed.equal('controllers.application.currentPath', 'panel.review')
