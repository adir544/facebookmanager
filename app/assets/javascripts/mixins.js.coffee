# reopen em.textfield to add autofocus attribute
Em.TextField.reopen(
  attributebindings: ['autofocus', 'required', 'pattern', 'title']
)

Em.Handlebars.helper 'datetime', (utc) ->
  moment(utc * 1000).format("DD/MM/YYYY HH:mm")

Em.Handlebars.helper 'ago', (utc) ->
  moment(utc * 1000).fromNow()

Fb.notify = (message, type) ->
  $.notify message, type
  return
