class GroupsController < LoggedController
  def create
    group = Group.create!(
      owner_app_user: current_app_user,
      name: params.require(:group)[:name]
    )
    render json: group
  end

  def index
    render json: current_app_user.groups, each_serializer: GroupSerializer
  end

  def add_users
    group = current_app_user.groups.find(params[:group_id])
    group.app_users << current_app_user.app.app_users.find(params[:users])
    render nothing: true
  end

  def remove_users
    group = current_app_user.groups.find(params[:group_id])
    app_users = group.app_users.find params[:users]
    group.app_users.delete app_users
    render nothing: true
  end

  def destroy
    group = current_app_user.groups.find(params[:id])
    group.delete()

    render nothing: true
  end
end
