class ChangeGroupUserColumn < ActiveRecord::Migration
  def change
    rename_column :groups, :owner_id, :owner_app_user_id
    rename_column :group_users, :user_id, :app_user_id
  end
end
