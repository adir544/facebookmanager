Fb.ManageAppView = Fb.ActionView.extend
  didInsertElement: ->
    # Setup details modal
    @$('.details').magnificPopup
      midClick: true
      items:
        src: '#details-modal'
        type: 'inline'

    # Select app link on click
    @$('.app-link').click ->
      @select()
