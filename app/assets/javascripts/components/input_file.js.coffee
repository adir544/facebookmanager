Fb.InputFile = Em.Component.extend
  tagName: "input"
  type: "file"
  attributeBindings: [
    "name"
    "id"
    "type"
    "required"
    "accept"
  ]

  change: (e) ->
    files = e.target.files
    @set('files', files)
    if files.length > 0
      @set('selectedFile', files[0])

Em.Handlebars.helper "input-file", Fb.InputFile
