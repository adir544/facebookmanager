class CreateJobUsersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :jobs, :app_users
  end
end
