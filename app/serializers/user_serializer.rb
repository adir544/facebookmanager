class UserSerializer < ActiveModel::Serializer
  embed :ids, embed_in_root: true
  attributes :id, :name, :fb_id, :picture_url, :fb_data, :created_at

  def fb_data
    JSON.load object.fb_data
  end

  def created_at
    object.created_at.to_i
  end
end
