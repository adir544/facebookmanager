Fb.WorkersLogView = Em.View.extend
  didInsertElement: ->
    # Scroll log buffer to bottom
    pre = document.querySelector '.workers-log pre'
    pre.scrollTop = pre.scrollHeight
