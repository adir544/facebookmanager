Fb.ExtRadioComponent = Em.Component.extend
  name: "radio"
  layoutName: 'components/ext_radio'

  mappedOptions: (->
    optionLabelPath = @get('optionLabelPath')
    optionValuePath = @get('optionValuePath')
    options = @get('options') or []
    return options.map (option, index) =>
      id: "#{@get('name')}-#{index}"
      label: if optionLabelPath? then Em.get(option, optionLabelPath) else option
      value: if optionValuePath? then Em.get(option, optionValuePath) else option
  ).property('options', 'optionLabelPath')
