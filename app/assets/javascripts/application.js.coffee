#= require query-2.1.1.min
#= require moment.min
#= require handlebars
#= require magnific.min
#= require jquery.blockUI
#= require notify.min
#= require ember
#= require ember-data
#= require_self
#= require ./mixins
#= require ./store
#= require_tree ./views
#= require_tree ./controllers
#= require ./components/ext_radio.js
#= require_tree ./components
#= require_tree ./templates
#= require_tree ./mixins
#= require_tree ./routes
#= require ./router

window.Fb = Ember.Application.create()

Fb.ObjectController = Em.ObjectController.extend
  needs: 'application'
  loggedUser: Em.computed.alias('controllers.application.loggedUser')
  loggedAppUser: Em.computed.alias('controllers.application.loggedAppUser')
  userApp: Em.computed.alias('controllers.application.userApp')
