Fb.SearchableInput = Em.Component.extend
  layoutName: 'components/searchable_input'
  classNames: 'searchable-input'

  input: (e) ->
    @set('value', e.target.value)

  didInsertElement: ->
    @$('.clear-button').click (e) =>
      @$('input').val('')
      @set('value', '')

Em.Handlebars.helper "searchable-input", Fb.SearchableInput
