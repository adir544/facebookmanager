Fb.ManageAppRoute = Em.Route.extend
  model: ->
    @controllerFor('application').get('userApp')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('selectedUser', model.get('appUsers.firstObject'))
    model.get('appUsers').forEach (appUser) =>
      appUser.addObserver('userType', @onAppUserTypeChanged)
    return

  onAppUserTypeChanged: (appUser) ->
    userId = appUser.get('user.id')
    userType = appUser.get('userType')
    $.post("/users/#{userId}/change_type", user_type: userType)

  actions:
    removeUser: (appUser) ->
      return unless confirm("Are you sure you want to remove \"#{appUser.get('user.name')}\"?")
      appUser.deleteRecord()
      id = appUser.get('id')
      $.ajax({type: 'DELETE', url: "/app_users/#{id}"})
      return
