class GroupSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :name, :owner_app_user_id
  has_many :app_users, each_serializer: BasicAppUserSerializer
end
