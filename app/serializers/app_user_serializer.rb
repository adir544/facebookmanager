class AppUserSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :user_type, :app_id
  has_one :user
  has_many :groups
end
