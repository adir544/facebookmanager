FROM ruby:2.2.0

# Install libpq-dev for PostgreSQL client gem compilation
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

# Install NodeJS for Rails ExecJS
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash - ; \
  apt-get install -y nodejs

# Obtain gems in a cached way
WORKDIR /tmp
COPY Gemfile* /tmp/
RUN bundle install

# /app will be the Rails app directory
RUN mkdir /app
WORKDIR /app
ADD . /app

EXPOSE 3000
CMD bundle exec thin start
