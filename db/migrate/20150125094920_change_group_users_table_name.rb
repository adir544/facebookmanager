class ChangeGroupUsersTableName < ActiveRecord::Migration
  def change
    rename_table :group_users, :app_users_groups
  end
end
