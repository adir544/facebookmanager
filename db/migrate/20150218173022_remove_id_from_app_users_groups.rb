class RemoveIdFromAppUsersGroups < ActiveRecord::Migration
  def change
      remove_column :app_users_groups, :id
  end
end
