Fb.PanelRoute = Em.Route.extend
  model: ->
    # This model will represent the composed action.
    {
      selectedAction: null,
      selectedUsers: [],
      delay: 0,
      delayBetweenUsers: 30
    }
