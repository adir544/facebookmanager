Fb.PanelIndexView = Em.View.extend
  selectedActionChanged: (->
    selectedAction = @get('controller.selectedAction')
    if selectedAction?
      Em.run.scheduleOnce 'afterRender', this, ->
        container = @get('actionView')
        container.clear()
        actionClassName = selectedAction.name.classify()
        actionView = Fb["#{actionClassName}View"].create()
        container.pushObject(actionView)
        @set('controller.selectedActionView', actionView)
  ).observes('controller.selectedAction').on('didInsertElement')
