Fb.PanelReviewController = Fb.ObjectController.extend
  shouldShowDelayBetweenUsers: Em.computed.gt('selectedUsers.length', 1)
  shouldShowTotalActionTime: Em.computed.gt('totalActionTime', 0)
  box: Em.computed.alias('selectedAction.box')

  numberOfUsersSelected: (->
    count = @get('selectedUsers.length')
    if count is 1 then 'one user' else "#{count} users"
  ).property('selectedUsers')

  reviewPartialName: (->
    "#{@get('selectedAction.name').underscore()}_review"
  ).property('selectedAction')

  totalActionTime: (->
    parseInt (@get('selectedUsers.length') - 1) * (@get('delayBetweenUsers') / 60)
  ).property('delayBetweenUsers', 'selectedUsers')

  estimatedCompletionTime: (->
    minutesToCompletion = (@get('totalActionTime') + parseInt(@get('delay'))) + 1
    endTime = moment().add(minutesToCompletion * 60 * 1000)
    formattedEndTime = endTime.format('DD.MM.YYYY HH:mm:ss')
    "#{formattedEndTime} (#{endTime.fromNow()})"
  ).property('totalActionTime', 'delay')
