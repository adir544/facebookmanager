Fb.PanelSelectUsersController = Fb.ObjectController.extend
  groupAllUsersSelected: Em.computed.equal('selectedGroup.name', 'All Users')
  allUsers: Em.computed.alias('userApp.users')

  blocked: (->
    @get('selectedUsers.length') <= 0
  ).property('selectedUsers.length')

  availableUsers: (->
    selectedGroup = @get('selectedGroup')
    return unless selectedGroup?
    @get('userApp.appUsers').filter (appUser) ->
      !selectedGroup.get('appUsers')?.contains(appUser)
  ).property('userApp.appUsers.[]', 'selectedGroup', 'selectedGroup.appUsers.[]')

  groups: (->
    loggedAppUser = @get('loggedAppUser')
    userApp = @get('userApp')
    groups = [Em.Object.create({name: 'All Users', appUsers: userApp.get('appUsers')})]
    userGroups = loggedAppUser.get('groups')
    if userGroups.get('length') > 0
      groups.pushObjects(userGroups.toArray())
    groups
  ).property('loggedAppUser.groups.[]', 'userApp.users')

  filteredUsers: (->
    selectedGroupUsers = @get('selectedGroupUsers')
    searchUsername = @get('searchUsername')
    if searchUsername? and searchUsername.length > 0
      selectedGroupUsers.filter (appUser) ->
        new RegExp(searchUsername, 'i').test appUser.get('user.name')
    else
      selectedGroupUsers
  ).property('searchUsername', 'selectedGroupUsers')

  selectedGroupUsers: (->
    @get('selectedGroup.appUsers')?.toArray()
  ).property('selectedGroup', 'selectedGroup.appUsers.[]')
