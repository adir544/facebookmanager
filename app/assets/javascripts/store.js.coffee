Fb.ApplicationAdapter = DS.ActiveModelAdapter

attr = DS.attr
hasMany = DS.hasMany
belongsTo = DS.belongsTo

Fb.RawTransform = DS.Transform.extend(
  deserialize: (serialized) ->
    return serialized

  serialize: (deserialized) ->
    return deserialized
)

Fb.User = DS.Model.extend
  name: attr('string')
  fbId: attr('string')
  apps: hasMany('app')
  pictureUrl: attr('string')
  fbData: attr('raw')
  userType: attr('string')
  createdAt: attr('number')

  isAdmin: Em.computed.equal('userType', 'admin')

Fb.App = DS.Model.extend
  name: attr('string')
  appUsers: hasMany('appUser')
  statuses: attr('raw')
  photos: attr('raw')

  users: Em.computed.mapBy('appUsers', 'user')

Fb.AppUser = DS.Model.extend
  user: belongsTo('user')
  app: belongsTo('app')
  userType: attr('string')
  fbToken: attr('string')
  fbTokenCreatedAt: attr('number')
  groups: hasMany('group', inverse: 'ownerAppUser')

  isManager: Em.computed.equal('userType', 'manager')

Fb.Group = DS.Model.extend
  name: attr('string')
  ownerAppUser: belongsTo('appUser')
  appUsers: hasMany('appUser')
