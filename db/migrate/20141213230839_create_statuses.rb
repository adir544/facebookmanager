class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :fb_id
      t.references :user
      t.references :app
      t.text :message
      t.datetime :posted_at
    end
  end
end
