Fb.LikeStatusView = Fb.ActionView.extend
  templateName: 'like_status'

  valid: (->
    @get('controller.box.selectedStatus') and $('form')[0].checkValidity()
  ).property('controller.box.selectedStatus')

  userStatuses: (->
    loggedUser = @get('controller.loggedUser')
    userApp = @get('controller.userApp')
    [
      {
        name: 'Your statuses',
        statuses: loggedUser.get('fbData.statuses'),
      },
      {
        name: 'Statuses posted via Social Manager',
        statuses: userApp.get('statuses')
      }
    ]
  ).property('controller.loggedUser.fbData.statuses')
