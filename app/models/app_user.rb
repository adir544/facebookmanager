class AppUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :app
  has_many :groups, foreign_key: :owner_app_user_id
  has_and_belongs_to_many :jobs
  has_many :app_photos, class_name: Photo

  include Facebook

  def update_fb_data
    fb_data = {
      statuses: self.statuses,
      albums: self.albums,
      events_created: self.events_created,
      tagged_videos: self.tagged_videos,
      videos: self.videos,
      photos: self.photos
    }
    self.user.update! fb_data: JSON.dump(fb_data)
  end

  def update_info
    info = self.info
    self.user.update!({
      name: info['name'],
      fb_id: info['id'],
      picture_url: info['picture'],
      birthday: (Date.strptime(info['birthday'], '%m/%d/%Y') rescue nil)
    })
  end
end
