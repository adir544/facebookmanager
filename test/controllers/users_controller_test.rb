require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include FactoryGirl::Syntax::Methods

  test "should post status" do
    signin do
      post :perform, {action_name: :post_status, message: 'hey'}
      assert_response :success
    end
  end

  test "should like status" do
    signin do
      post :perform, {action_name: :like_status, status_ids: '1'}
      assert_response :success
    end
  end

  test "should like multiple statuses" do
    signin do
      post :perform, {action_name: :like_status, status_ids: '1,2,3'}
      assert_response :success
    end
  end
end
