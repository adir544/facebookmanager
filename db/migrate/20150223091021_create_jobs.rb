class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.references :manager, index: true, null: false
      t.string :job_state, null: false
      t.string :action_name, null: false
      t.json :action_params, null: false
      t.float :job_delay, null: false
      t.float :delay_between_users, null: false
      t.string :sidekiq_jid

      t.timestamps
    end
  end
end
