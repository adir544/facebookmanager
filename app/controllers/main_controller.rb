require 'httparty'

class MainController < ApplicationController
  REQUESTED_FACEBOOK_SCOPE = 'user_about_me,user_actions.books,user_actions.fitness,user_actions.music,user_actions.news,user_actions.video,user_birthday,user_education_history,user_events,user_friends,user_games_activity,user_hometown,user_likes,user_location,user_photos,user_relationship_details,user_relationships,user_religion_politics,user_status,user_tagged_places,user_videos,user_website,user_work_history,ads_management,ads_read,email,manage_pages,publish_actions,read_insights,read_page_mailboxes,rsvp_event,user_posts,user_videos'

  class InvalidAuthenticationCode < RuntimeError
  end

  def main
    # This is where the authentication process begins.
    # We start with the app_id parameter.
    # It should be provided by the user, if not, an error message will be displayed.
    app_id = params[:app_id]
    if app_id.nil?
      render text: 'The app_id parameter was not specified' and return
    end
    app = App.find(app_id)

    # If we're in development mode we can spare the upcoming OAuth journey
    if Rails.env.development? and params[:user_id]
      user = User.find(params[:user_id])
      app_user = app.app_users.find_by_user_id!(user.id)
      session['app_user_id'] = app_user.id
      return
    end

    # If the user is logged in, we can render the panel for him, if he's
    # authorized.
    if current_app_user.present? and current_app_user.app.id == app_id.to_i
      if current_app_user.user_type != 'manager' and current_user.user_type != 'admin'
        render text: 'Application installed successfully' and return
      end
    else
      # Redirect the user to Facebook to acquire an auth code
      dialog_params = {
        client_id: app.fb_app_id,
        redirect_uri: redirect_uri(app),
        scope: REQUESTED_FACEBOOK_SCOPE
      }
      redirect_to "https://www.facebook.com/v2.5/dialog/oauth?#{dialog_params.to_query}"
    end
  end

  def auth
    # This route is the second step in the autentication process.
    # If the error_code parameter was provdied, then something gone wrong.
    # If the code parameter was provdied, then we can acquire the user access token.
    # If no parameter was provdied, then probably someone is trying to "challenge" us.
    code = params[:code]
    app_id = params[:app_id]
    if params[:error_code].present?
      render text: "Error: #{params.to_s}", content_type: Mime::TEXT and return
    elsif app_id.nil?
      render text: 'Missing app id', content_type: Mime::TEXT and return
    elsif code.nil?
      render text: 'Missing code', content_type: Mime::TEXT and return
    end

    app = App.find(app_id)

    # Now that we got an auth code, we need to redirect the user to
    # Facebook in order to acquire an access token.
    begin
      access_token = acquire_access_token(app, code)
    rescue InvalidAuthenticationCode
      render partial: 'invalid_auth_code' and return
    end

    # OK, we've got a valid access token, now we can identify our user
    # by querying Facebook API with our new access token.
    user_info = AppUser.new(fb_token: access_token).info

    # Inside user_info we have the user Facebook ID, lets query our
    # DB to see if we know the user
    user = User.find_by_fb_id(user_info['id'])
    app_user = nil
    if user.present?
      # This user is well known, does he belongs to the requested app?
      # If so, update his access token.
      # If not, add him as a new normal user to the app.
      app_user = AppUser.find_by(app_id: app.id, user_id: user.id)
      if app_user.present?
        app_user.update!(
          fb_token: access_token,
          fb_token_created_at: Time.now,
        )
      else
        app_user = AppUser.create!({
          app: app,
          user: user,
          fb_token: access_token,
          fb_token_created_at: Time.now,
          user_type: 'normal'
        })
      end
    else
      # This is a new user, lets add him as a normal user to the app
      user = User.create!({
        name: user_info['name'],
        fb_id: user_info['id'],
        picture_url: user_info['picture'],
        birthday: (Date.strptime(user_info['birthday'], '%m/%d/%Y') rescue nil)
      })
      app_user = AppUser.create!({
        app: app,
        user: user,
        fb_token: access_token,
        fb_token_created_at: Time.now,
        user_type: 'normal'
      })

      # Lets cache the user Facebook data(statuses, photos, videos, etc...)
      app_user.update_fb_data()
    end

    session['app_user_id'] = app_user.id
    redirect_to "/?app_id=#{app.id}"
  end

  def sidekiq_log
    render text: File.read('log/sidekiq.log'), content_type: Mime::TEXT
  end

  def signout
    reset_session
    redirect_to 'https://facebook.com'
  end

  private

  def redirect_uri(app)
    "https://socialmanager.club/auth?app_id=#{app.id}"
  end

  def acquire_access_token(app, code)
    access_token_params = {
      client_id: app.fb_app_id,
      client_secret: app.fb_app_secret,
      code: code,
      display: 'popup',
      redirect_uri: redirect_uri(app)
    }
    fb_response = HTTParty.get(
      "https://graph.facebook.com/v2.5/oauth/access_token?#{access_token_params.to_query}"
    )
    if fb_response.code != 200
      raise InvalidAuthenticationCode
    end
    access_token = fb_response.parsed_response['access_token']
    access_token
  end

end
