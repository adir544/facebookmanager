class UserManager < ActiveRecord::Base
  belongs_to :manager, class_name: 'User', inverse_of: :manager
  belongs_to :user
end
