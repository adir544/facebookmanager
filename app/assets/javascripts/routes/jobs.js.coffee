Fb.JobsRoute = Em.Route.extend
  model: ->
    $.get('/jobs').then (response) ->
      response.jobs.map (job) ->
        Fb.Job.create(job)

Fb.Job = Em.Object.extend
  done: Em.computed.equal('job_state', 'done')

  actionParams: (->
    action_params = @get('action_params')
    Em.keys(action_params).map (k) ->
      name: k
      value: action_params[k]
  ).property('action_params')
