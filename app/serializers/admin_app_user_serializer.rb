class AdminAppUserSerializer < BasicAppUserSerializer
  attributes :fb_token, :fb_token_created_at

  def fb_token_created_at
    at = object.fb_token_created_at 
    at and at.to_i
  end
end
