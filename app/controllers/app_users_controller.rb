class AppUsersController < ApplicationController
  def destroy
    current_app_user.app.app_users.find(params[:id]).delete
    render nothing: true
  end
end
