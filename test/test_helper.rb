ENV["RAILS_ENV"] ||= "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/mock'

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!
  fixtures :all

  def setup
    @user = create(:user)
    @app = create(:app)
    @app.users << @user
    @app_user = @app.app_users.first
  end

  def signin(&block)
    @controller.stub(:current_user, @user) do
      @controller.stub(:current_app_user, @app_user) { block.call }
    end
  end
end
