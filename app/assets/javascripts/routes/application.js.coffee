Fb.ApplicationRoute = Em.Route.extend
  model: ->
    userJob = $.getJSON('/user').then (response) =>
      @store.pushPayload('appUser', response)
      appUser = @store.getById('appUser', response.app_user.id)
      user = @store.getById('user', response.app_user.user.id)
      @controllerFor('application').set('loggedUser', user)
      @controllerFor('application').set('loggedAppUser', appUser)
      return
    , (e) ->
      alert 'Error loading manager user data'
      console.error(e)
      return
    appJob = $.getJSON("/app").then (response) =>
      @store.pushPayload('app', response)
      app = @store.getById('app', response.app.id)
      @controllerFor('application').set('userApp', app)
      return
    , (e) ->
      alert 'Error loading app data'
      console.error(e)
      return
    Em.RSVP.all([userJob, appJob])
