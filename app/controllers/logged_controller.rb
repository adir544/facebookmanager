class LoggedController < ApplicationController
  prepend_before_filter :authorize_user

  private

  def authorize_user
    # Renders unauthorized with status code 401 when the current user isn't logged in.
    if current_user.blank?
      render nothing: true, status: :unauthorized
    end
  end

end
