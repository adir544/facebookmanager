Fb.PanelReviewRoute = Em.Route.extend Fb.ActionsMixin,
  model: (_, transition) ->
    model = @modelFor 'panel'
    if model.selectedAction? and model.selectedUsers.length > 0
      model
    else if model.selectedAction? and model.selectedUsers.length is 0
      transition.abort()
    else
      @replaceWith 'panel'

  actions:
    confirm: ->
      # call the action method, for example if the selected action is "post-status",
      # then this method will invoke the actions.postStatus method
      @send(@get('controller.selectedAction.name').camelize())
      return
