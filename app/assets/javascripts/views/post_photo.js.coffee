Fb.PostPhotoView = Fb.ActionView.extend
  templateName: 'post_photo'

  valid: (->
    $('form')[0].checkValidity()
  ).property('controller.box.photoFile')

  selectedFileChanged: (->
    photoFile = @get('controller.box.photoFile')
    if photoFile
      # TODO: probably should use only one reader and cancel previous reads
      reader = new FileReader()
      reader.onloadend = =>
        @set('controller.box.photoPreviewURL', reader.result)
      reader.readAsDataURL(photoFile)
  ).observes('controller.box.photoFile')
