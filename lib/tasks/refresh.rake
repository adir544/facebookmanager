task refresh: [:environment] do
  AppUser.all.each do |app_user|
    begin
      puts "Refreshing AppUser #{app_user.id} - #{app_user.user.name}"
      app_user.update_info
      app_user.update_fb_data
    rescue
      puts "Error: #{$!.message}"
      next
    end
  end
end
