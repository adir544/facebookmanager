Fb.Router.map ->
  @route 'panel', path: '/', ->
    @route 'select_users', path: '/select_users'
    @route 'review', path: '/review'
  @route 'manage_app', path: '/manage_app'
  @route 'completed', path: '/completed'
  @route 'birthdays', path: '/birthdays'
  @route 'workers_log', path: '/worker_logs'
  @route 'jobs', path: '/jobs'
