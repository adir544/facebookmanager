Fb.LikePhotoView = Fb.ActionView.extend
  templateName: 'like_photo'

  valid: (->
    # check if the user or the app have any photos
    userPhotos = @get('controller.loggedUser.fbData.photos')
    appPhotos = @get('controller.userApp.photos')
    if userPhotos.get('length') <= 0 and appPhotos.get('length') <= 0
      return false
    $('form')[0].checkValidity()
  ).property('controller.loggedUser.fbData.photos.[]', 'controller.userApp.photos', 'controller.box.selectedPhoto')

  userHasPhotos: (->
    loggedUser = @get('controller.loggedUser')
    (loggedUser.get('fbData.photos.length') > 0 or
     loggedUser.get('fbData.albums.length') > 0)
  ).property('controller.loggedUser.fbData.{albums,photos}')

  userAlbums: (->
    albums = []
    loggedUser = @get('controller.loggedUser')
    userApp = @get('controller.userApp')
    albums.pushObjects(loggedUser.get('fbData.albums'))

    # we need to create a new album for tagged photos
    taggedPhotos =
      name: 'Tagged photos'
      photos: loggedUser.get('fbData.photos')
    albums.insertAt(0, taggedPhotos)

    # Add "posted via SocialManager" album
    postedPhotos =
      name: 'Posted via SocialManager'
      photos: userApp.get('photos')
    albums.insertAt(0, postedPhotos)

    albums
  ).property('controller.loggedUser.fbData.albums')
