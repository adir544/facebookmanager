Fb.ManageAppController = Fb.ObjectController.extend
  sortProperties: ['id:desc']
  sortedUsers: Em.computed.sort('appUsers', 'sortProperties')
  userTypeOptions: ['normal', 'manager']

  appLink: (->
    "https://#{location.host}/?app_id=#{@get('id')}"
  ).property('id')

  actions:
    setUser: (appUser) ->
      @set('selectedUser', appUser)
      return
