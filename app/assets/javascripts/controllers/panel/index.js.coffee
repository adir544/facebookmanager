Fb.PanelIndexController = Fb.ObjectController.extend
  needs: 'application'
  actionViewContainer: Em.ContainerView.extend()

  actions: [
    {name: 'Post', subActions: [
      {name: 'post-status', label: 'Status', box: {}}
      {name: 'post-photo', label: 'Photo', box: {}}
    ]},
    {name: 'Like', subActions: [
      {name: 'like-status', label: 'Status', box: {}}
      {name: 'like-comment', label: 'Comment', box: {}}
      {name: 'like-photo', label: 'Photo', box: {}}
      {name: 'like-video', label: 'Video', box: {}}
    ]},
    {name: 'Attend', subActions: [
      {name: 'attend-event', label: 'Event', box: {}}
    ]}
  ]

  box: (->
    @get('selectedAction.box')
  ).property('selectedAction')

  blocked: (->
    selectedActionView = @get('selectedActionView')
    !(selectedActionView? and selectedActionView.get('valid'))
  ).property('selectedActionView.valid')

  onSelectedTopActionChange: (->
    @set('selectedAction', null)
  ).observes('selectedTopAction')
