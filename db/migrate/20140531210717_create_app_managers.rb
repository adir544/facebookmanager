class CreateAppManagers < ActiveRecord::Migration
  def change
    create_table :app_managers do |t|
      t.belongs_to :app
      t.belongs_to :manager

      t.timestamps
    end
  end
end
