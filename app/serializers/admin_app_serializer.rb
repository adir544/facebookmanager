class AdminAppSerializer < AppSerializer
  root :app
  has_many :app_users, each_serializer: AdminAppUserSerializer
end
