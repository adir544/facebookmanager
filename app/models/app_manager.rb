class AppManager < ActiveRecord::Base
  belongs_to :app
  belongs_to :manager, class_name: 'User'
end
