class BasicAppUserSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :user_type, :app_id
  has_one :user, serializer: BasicUserSerializer
end
