Fb.RadioButton = Em.Component.extend
  tagName: "input"
  type: "radio"
  attributeBindings: [
    "name"
    "id"
    "type"
    "value"
    "required"
    "checked:checked"
  ]

  label: (->
    @get("value.#{@get('optionLabelPath')}")
  ).property('optionLabelPath')

  click: ->
    @set "selection", @get('value')
    return

  checked: (->
    @get('selection') is @get('value')
  ).property('selection')

Em.Handlebars.helper "radio-button", Fb.RadioButton
