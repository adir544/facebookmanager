require 'sidekiq/web'

Fb::Application.routes.draw do
  root 'main#main'
  get '/auth', to: 'main#auth'
  mount Sidekiq::Web => '/sidekiq'
  get 'sidekiq_log', controller: :main
  get 'signout', controller: :main

  # Logged user routes
  resource :user, only: [:show] do
    post :show, action: :update
    post :perform
  end

  resources :users, only: [:destroy] do
    post :change_type
  end

  resources :app_users, only: [:destroy]

  resources :jobs, only: [:index]
  
  resources :groups, only: [:create, :destroy] do
    post :add_users
    post :remove_users
  end

  resource :app, only: [:show]
  get 'birthdays' => 'users#birthdays'
end
