Fb.WorkersLogRoute = Em.Route.extend
  model: ->
    $.get('sidekiq_log').then (log) ->
      Em.Object.create(
        logContent: log
        linesFilter: 30
      )
