Fb.WorkersLogController = Em.ObjectController.extend
  workersLog: (->
    log = @get('logContent')
    lines = log.split('\n')

    # Slice lines
    numberOfLines = @get('linesFilter')
    if numberOfLines
      lines = lines.slice(-numberOfLines)

    # Filter matches
    filter = @get('logFilter') or ''
    if filter.length > 0
      lines = lines.filter((line) ->
        line.match(new RegExp(filter, 'i'))
      )

    lines.join('\n')
  ).property('linesFilter', 'logFilter', 'content')
