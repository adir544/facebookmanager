FactoryGirl.define do
  factory :user do
    name 'Local Testi'
    fb_id '123abcdf'
    picture_url 'http://no'
  end

  factory :app do
    name 'MyApp'
    fb_app_id '1'
    fb_app_secret 'shhh'
  end
end
