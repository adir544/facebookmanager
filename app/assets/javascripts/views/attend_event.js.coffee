FakeEvent = Em.Object.extend
  name: (->
    "Event ##{@get('id')}"
  ).property('id')

Fb.AttendEventView = Fb.ActionView.extend
  templateName: 'attend_event'
  eventTypes: ['Published Events', 'Event ID']
  showPublishedEvents: Em.computed.equal('selectedEventType', 'Published Events')
  showEventId: Em.computed.equal('selectedEventType', 'Event ID')
  fakeEvent: FakeEvent.create()

  onEventTypeChange: (->
    if @get('showEventId')
      @set('controller.box.selectedEvent', @get('fakeEvent'))
    else
      @set('controller.box.selectedEvent', null)
  ).observes('selectedEventType')

  valid: (->
    if @get('showPublishedEvents')
      @get('controller.box.selectedEvent') and $('form')[0].checkValidity()
    else if @get('showEventId')
      @get('fakeEvent.id') and $('form')[0].checkValidity()
    else
      false
  ).property('selectedEventType', 'fakeEvent.id', 'controller.box.selectedEvent')
