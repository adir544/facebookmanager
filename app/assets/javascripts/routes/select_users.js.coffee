Fb.PanelSelectUsersRoute = Em.Route.extend
  model: ->
    model = @modelFor 'panel'
    if model.selectedAction?
      model
    else
      @replaceWith 'panel'

  actions:
    continue: ->
      @transitionTo 'panel.review'
      return

    createGroup: ->
      newGroup = @store.createRecord 'group',
        name: @get('controller.newGroupName')
        owner: @get('controller.loggedUser')
      newGroup.save().then((groupCreated) =>
        @set('controller.selectedGroup', groupCreated)
        $('.modal.new-group #group-name').val('')
        Fb.notify 'Group Created', 'success'
        $.magnificPopup.close()
      , (ex) ->
        Fb.notify "error: #{ex}", 'error'
      )
      return

    addUsersToGroup: ->
      selectedUsers = @get('controller.selectedUsersToAdd')
      selectedGroup = @get('controller.selectedGroup')
      selectedGroup.get('appUsers').pushObjects(selectedUsers)
      $.magnificPopup.close()

      payload =
        users: selectedUsers.mapBy('id')
      $.post("/groups/#{selectedGroup.get('id')}/add_users", payload).then(->
        Fb.notify 'Users added successfully!', 'success'
      , (ex) ->
        Fb.notify "error: #{ex}", 'error'
      )
      return

    removeUsersFromGroup: ->
      selectedUsers = @get('controller.selectedUsersToRemove')
      selectedGroup = @get('controller.selectedGroup')
      selectedGroup.get('appUsers').removeObjects(selectedUsers)
      $.magnificPopup.close()

      payload =
        users: selectedUsers.mapBy('id')
      $.post("/groups/#{selectedGroup.get('id')}/remove_users", payload).then(->
        Fb.notify 'Users removed successfully!', 'success'
      , (ex) ->
        Fb.notify "error: #{ex}", 'error'
      )
      return

    deleteGroup: ->
      controller = @get('controller')
      selectedGroup = controller.get('selectedGroup')
      selectedGroup.deleteRecord()
      $.magnificPopup.close()

      $.ajax("/groups/#{selectedGroup.get('id')}", {
        type: 'DELETE',
        data: id: selectedGroup.get('id')
      }).then(->
        Fb.notify 'Group deleted successfully', 'success'
        controller.set('selectedGroup', controller.get('groups.firstObject'))
      , (ex) ->
        Fb.notify "error: #{ex}", 'error'
      )
      return

    selectAll: ->
      @get('controller.userSelector').selectAll()
      return

    selectNone: ->
      @get('controller.userSelector').selectNone()
      return
