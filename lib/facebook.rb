require 'httparty'
module Facebook
  FACEBOOK_API_URL = 'https://graph.facebook.com/v2.5'

  class FacebookError < RuntimeError; end
  class InvalidAccessTokenError < FacebookError; end

  def request(method, url, params)
    params[:access_token] = self.fb_token
    response = HTTParty.send(method, "#{FACEBOOK_API_URL}/#{url}?#{params.to_query}", format: :json)
    if response.code == 200
      response
    else
      error_code = response.parsed_response['error']['code']
      error_message = response.parsed_response['error']['message']
      raise case error_code
      when 2500, 190
        InvalidAccessTokenError.new error_message
      else
        FacebookError.new error_message
      end
    end
  end

  def get(url, params={})
    request :get, url, params
  end

  def post(url, params={})
    request :post, url, params
  end

  # user actions

  def post_status(message)
    response = post 'me/feed', message: message
    response.parsed_response['id']
  end

  def post_photo(photo_url, message)
    response = post 'me/photos', {url: photo_url, message: message}
    response.parsed_response['id']
  end

  def like(object_id)
    post "#{object_id}/likes"
  end

  def attend_event(event_id)
    post "#{event_id}/attending"
  end

  # query user functions

  def statuses
    response = get 'me/feed', fields: 'story,message,type'
    response.parsed_response['data'].keep_if {|story| story['type'] == 'status'}
    response.parsed_response['data'].map do |status|
      {
        id: status['id'],
        message: status['message'] || status['story']
      }
    end
  end

  def tagged_videos
    response = get 'me/videos', fields: 'id,picture'
    response.parsed_response['data']
  end

  def videos
    response = get 'me/videos/uploaded', fields: 'id,picture'
    response.parsed_response['data']
  end

  def photos
    response = get 'me/photos', fields: 'name,id,picture'
    response.parsed_response['data']
  end

  def albums
    response = get 'me/albums', fields: 'id,name,photos{id,picture,name}'
    albums = response.parsed_response['data']
    # make the data property the value of each album
    albums.map! do |album|
      {
        id: album['id'],
        name: (album['name'] rescue 'Unnamed album'),
        photos: (album['photos']['data'] rescue [])
      }
    end
  end

  def info
    response = get 'me', fields: 'id,name,picture,birthday'
    result = response.parsed_response
    result['picture'] = result['picture']['data']['url']
    result
  end

  def events_created
    response = get 'me/events/created', fields: 'id,name'
    response.parsed_response['data']
  end

  def has_valid_token?
    return false if self.fb_token.empty?
    begin
      get 'me'
      true
    rescue InvalidAccessTokenError
      false
    end
  end

end
