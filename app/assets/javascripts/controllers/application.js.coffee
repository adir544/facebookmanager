Fb.ApplicationController = Em.Controller.extend
  isAuthenticated: (->
    return @get('loggedUser')?
  ).property('loggedUser')

  loggedUser: null

  actions:
    reloadData: ->
      $.blockUI()
      $.post('/user').then (response) =>
        @store.pushPayload 'user', response
        $.unblockUI()
      return
