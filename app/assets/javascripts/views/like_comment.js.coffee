Fb.LikeCommentView = Fb.ActionView.extend
  templateName: 'like_comment'

  valid: (->
    $('form')[0].checkValidity() and @get('controller.box.selectedComment')?
  ).property('controller.box.{selectedStatus,selectedComment}')
